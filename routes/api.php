<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/containers', 'Controller@containers');
Route::get('/find-key', 'Controller@findKey');
Route::post('/order', 'Controller@order');
Route::get('/order/{id}', 'Controller@getOrderById');
Route::post('/order/{id}/process', 'Controller@orderProcess');
