<?php

namespace App\Increase\Library;

use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

trait Publisher
{
    protected static function sendRabbit($message, string $channelName = 'order')
    {
        if (is_array($message) or is_object($message)) {
            $message = json_encode($message);
        } elseif (!is_string($message)) {
            return false;
        }

        try {
            $connection = new AMQPStreamConnection(
                env('MQ_HOST', 'localhost'),
                env('MQ_PORT', 5672),
                env('MQ_USER', 'guest'),
                env('MQ_PASSWORD', 'guest'),
                env('MQ_USER', 'guest')
            );

            $channel = $connection->channel();
            $channel->queue_declare($channelName);
            $channel->basic_publish(new AMQPMessage($message), '', $channelName);
            $channel->close();
            $connection->close();
            return true;
        } catch (\Exception $e) {
            logger($e);
            Log::error($e);
            return false;
        }
    }
}