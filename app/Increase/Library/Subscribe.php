<?php

require_once 'vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\Http\Repositories\OnlineStoreRepository;
use PhpAmqpLib\Connection\AMQPStreamConnection;

const URL = 'http://localhost:8000/api/order/{id}/process';

$connection = new AMQPStreamConnection(
    'lion.rmq.cloudamqp.com',
    5672,
    'dynhxtdp',
    'NUo9Aa8t4jTzaagW-gZrtCdirEc_6zFj',
    'dynhxtdp'
);

$channel = $connection->channel();
$channel->queue_declare('order');

echo " [*] Waiting for messages. To exit press CTRL+C\n";

$data = $channel->basic_consume('order', '', false, true, false, false, function ($msg) {
    echo $msg->body;
    $params = [
        'id' => $msg->body
    ];
    $req = (new Client())->request('POST', 'http://localhost:8000/api/order/' . $msg->body . '/process');
});

while ($channel->is_consuming()) {
    $channel->wait();
}

$channel->close();
$connection->close();