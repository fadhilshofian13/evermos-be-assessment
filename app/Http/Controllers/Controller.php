<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Repositories\ContainerRepository;
use App\Http\Repositories\FindKeyRepository;
use App\Http\Repositories\OnlineStoreRepository;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{

    public function __construct(
        ContainerRepository $containerRepo,
        FindKeyRepository $findKeyRepo,
        OnlineStoreRepository $onlineStoreRepo
    ){
        $this->containerRepo = $containerRepo;
        $this->findKeyRepo = $findKeyRepo;
        $this->onlineStoreRepo = $onlineStoreRepo;
    }

    public function containers(Request $request)
    {
        try{
            $total = $request->number_of_containers;
            $size = $request->container_size;

            for($i=0; $i < $total; $i++){
                $containers[$i] = 0;
            }

            $response = $this->containerRepo->putBall($containers, $total, $size);
        } catch (\Exception $e) {
            return $e;
        }

        return $response;
    }

    public function findKey()
    {
        try{
            $map = array(
                array(1, 1, 1, 1, 1, 1, 1, 1),
                array(1, 0, 0, 0, 0, 0, 0, 1),
                array(1, 0, 1, 1, 1, 0, 0, 1),
                array(1, 0, 0, 0, 1, 0, 1, 1),
                array(1, 2, 1, 0, 0, 0, 0, 1),
                array(1, 1, 1, 1, 1, 1, 1, 1)
            );
    
            $rowJoni = 4;
            $columnJoni = 1;
            
            $response = $this->findKeyRepo->dotPossibility($map, $rowJoni, $columnJoni);
        } catch (\Exception $e) {
            return $e;
        }

        return $response;
    }

    public function order(Request $request)
    {
        try{
            $response = $this->onlineStoreRepo->order($request->id);
        } catch (\Exception $e) {
            return $e;
        }
        
        return $response;
    }

    public function orderProcess($id)
    {
        try{
            $response = $this->onlineStoreRepo->orderProcess($id);
        } catch (\Exception $e) {
            return $e;
        }
        
        return $response;
    }

    public function getOrderById($id)
    {
        try{
            $response = $this->onlineStoreRepo->getOrderById($id);
        } catch (\Exception $e) {
            return $e;
        }
        
        return $response;
    }
}
