<?php

namespace App\Http\Repositories;

class ContainerRepository
{
    public function putBall(array $containers, $numberOfContainer, $size)
    {
        $index = 0;

        logger($containers);

        while(!($this->isFull($containers[$index], $size))) {
            $index = mt_rand(0, count($containers) - 1);
            $containers[$index]++;
            logger($containers);
        }

        $result = [
            'containers' => $containers,
            'full_container_index' => $index
        ];

        return $result;
    }

    public function isFull($containers, $size)
    {
        if($containers < $size){
            return false;
        } else {
            return true;
        }
    }
}
