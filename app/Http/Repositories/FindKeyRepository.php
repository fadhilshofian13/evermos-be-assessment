<?php

namespace App\Http\Repositories;

class FindKeyRepository
{
    public function dotPossibility($map, $currentRow, $currentColumn)
    {
        $rowA = $currentRow - 1;
        $columnA = $currentColumn;
        $possibility['total'] = 0;
        $possibility['position'] = null;
        $index = 0;

        while($map[$rowA][$columnA] != 1){
            $rowB = $rowA;
            $columnB = $columnA + 1;
            while($map[$rowB][$columnB] != 1){
                $rowC = $rowB + 1;
                $columnC = $columnB;
                while($map[$rowC][$columnC] != 1){
                    $map[$rowC][$columnC] = 8;
                    $possibility['total']++;
                    $possibility['position'][$index] = $index+1 . ". [" . $rowC . "][" . $columnC . "]"; 
                    $index++;
                    $rowC++;
                }
                $columnB++;
            }
            $rowA--;
        }

        return $possibility;
    }
}