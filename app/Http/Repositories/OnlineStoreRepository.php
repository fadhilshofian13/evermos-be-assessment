<?php

namespace App\Http\Repositories;

use App\Product;
use App\Order;
use App\Increase\Library\Publisher;
use Illuminate\Support\Facades\DB;

class OnlineStoreRepository
{
    use Publisher;

    public function getOrderById($id)
    {
        $order = Order::where('id', $id)->first();
        return $order;
    }

    public function order($id)
    {
        $order['product_id'] = $id;
        $order['status'] = 1;
        $order = Order::create($order);

        $send = $this->sendRabbit(strval($order->id), 'order');

        if(!$send){
            $response = [
                'status' => $send,
                'message' => "Failed create order, please try again"
            ];

            return $response;
        }

        $response = [
            'status' => $send,
            'message' => "Success create order"
        ];

        return $response;
    }

    public static function orderProcess($orderId)
    {
        echo "masuk repo\n";
        $order = Order::where('id', $orderId)->first();
        $product = Product::where('id', $order->product_id)->first();

        if($product->qty > 0){
            $product->qty--;
            $product->save();

            $order->status = 2;
            $order->save();
        }   
    }
}